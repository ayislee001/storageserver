'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserFilesSchema extends Schema {
	up () {
		this.create('user_files', (table) => {
			table.increments()
			table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
			table.string('file_id',10).unique().notNullable()
			table.string('file_path').unique()
			table.string('file_type',10)
			table.string('content_type',50)
			table.integer('duration')
			table.text('json_info')
			table.timestamps()
		})
	}

	down () {
		this.drop('user_files')
	}
}

module.exports = UserFilesSchema
