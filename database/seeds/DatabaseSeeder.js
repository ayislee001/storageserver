'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use("Database")
const Moment = use("moment")

const Encryption = use('Encryption')
const bcrypt = use('bcryptjs')
const Env = use('Env')


class DatabaseSeeder {
	async run() {
		var salt = bcrypt.genSaltSync(10);
		await Database.table('users').insert(
			[
				{
					id: 1,
					email: "ayislee001@gmail.com",
					username: "arest",
					password: bcrypt.hashSync('123456', salt),
					root_dir: "arest/"
				},
				{
					id: 2,
					email: "admin@mediacartz.com",
					username: "mediacartz",
					password: bcrypt.hashSync('123456', salt),
					root_dir: "mediacartz/"
				},
				{
					id: 3,
					email: "admin@kolia.id",
					username: "kolia",
					password: bcrypt.hashSync('123456', salt),
					root_dir: "kolia/"
				},
				{
					id: 4,
					email: "admin@micindo.com",
					username: "micindo",
					password: bcrypt.hashSync('123456', salt),
					root_dir: "micindo/"
				}
			]
		)

		
	}
}

module.exports = DatabaseSeeder