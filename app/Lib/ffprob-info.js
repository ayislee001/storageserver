#!/usr/bin/env node

const { promisify } = require('util')
const exec = promisify(require('child_process').exec);

module.exports = main

async function main(filename) {

	const str = `ffprobe -show_streams -show_entries format=bit_rate,filename,start_time:stream=duration,width,height,display_aspect_ratio,r_frame_rate,bit_rate -of json -v quiet -i "${filename}"`;
	const { stdout } = await exec(str);
	console.log(str)
	return stdout;
}

if (!module.parent) {
	/* cli mode */
	main(process.argv.slice(2).join(' ')).then(console.log).catch(error => {
		console.error(error);
		process.exitCode = 1;
	});
}
