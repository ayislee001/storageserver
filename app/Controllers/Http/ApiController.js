'use strict'
const Drive = use("Drive")
const Env = use("Env")
const Helpers = use('Helpers')
const fs = use('fs')
const readFile = Helpers.promisify(fs.readFile)

const crypto = use('crypto-random-string')
const getDuration = use('ffprobe-duration')

const ffInfo = use('App/Lib/ffprob-info')

const Users = use('App/Models/User')
const UserFiles = use('App/Models/UserFile')



class ApiController {
	async store({ request, response, session, auth }) {
		console.log(request.post())
		const formData = request.only(["email", "password"])
		await auth.logout()


		try {
			await auth.attempt(formData.email, formData.password)

			await auth.logout()
			

		} catch (error) {
			console.log(error)
			await auth.logout()
			return response.json(
				{
					status: false,
					error: 'invalid credentials'
				}
			)
		}

		const user = await Users.query()
			.where('email',formData.email)
			.first()
		// return response.json(user)

		// check and create root folder exist
		const root_folder = await Drive.exists(Env.get('STORAGE_ROOT'))

		if(!root_folder){
			fs.mkdirSync(Env.get('STORAGE_ROOT'),777)
		}

		// check and create folder exist
		const user_folder = await Drive.exists(Env.get('STORAGE_ROOT')+user.username)
		
		if(!user_folder){
			fs.mkdirSync(Env.get('STORAGE_ROOT')+user.username,777)
		}

		const file = request.file("file")

		console.log(file)
		// return file
		const fn = crypto({length: 10, characters: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'});
		const f = await Drive.exists(file.tmpPath)

		// return response.json(f)
		let ext
		if(file.extname == 'quicktime'){
			ext = 'mov'
		}else{
			ext = file.extname
		}
		const move = await Drive.move(file.tmpPath,Env.get('STORAGE_ROOT')+  Env.get('PUBLIC_FOLDER') + user.username+'/'+fn+'.'+ext)
		if(move){
			let duration
			let info
			if(ext == 'mp4' || ext == 'mpg' || ext == 'mpeg' || ext == 'avi' || ext == 'mkv' || ext == 'mov' || ext == 'quicktime'){

				duration = await getDuration(Env.get('STORAGE_ROOT') +  Env.get('PUBLIC_FOLDER') + user.username+'/'+fn+'.'+ext)

				info = await ffInfo(Env.get('STORAGE_ROOT') +  Env.get('PUBLIC_FOLDER') + user.username+'/'+fn+'.'+ext)
			}else{
				info = null
				duration = 0
			}
			
			const uf = new UserFiles()
			uf.user_id = user.id 
			uf.file_id = fn 
			uf.file_path = Env.get('STORAGE_ROOT') + Env.get('PUBLIC_FOLDER') + user.username + '/' + fn + '.' + ext
			uf.duration = duration
			if(ext == 'mov' && file.headers['content-type'] == 'application/octet-stream'){
				uf.content_type = 'video/quicktime'
			}else{
				uf.content_type = file.headers['content-type']
			}
			
			uf.file_type = ext
			uf.json_info = info

			await uf.save()
			return response.json({
				status: true,
				data: {
					url: Env.get('APP_HOST') + Env.get('PUBLIC_FOLDER') + user.username + '/' + fn + '.' + ext,
					duration: duration,
					content_type: uf.content_type,
					info: JSON.parse(info)

				}

			})



		}else{
			return response.json({
				status : false,
				error : "Fail to staore file"
			})
		}
		

	}

	async file({ request, response, params }){
		const file = await UserFiles.query()
			.where('file_id',params.id)
			.first() 
		
		if(file == null){
			return response.json({
				status: false,
				error: 'Data not found'
			})
		}
		// return response.json(file)
		response.header('Content-type', file.content_type)
		const f = await readFile(file.file_path)
		return response.send(f)
		// return f
	}
}

module.exports = ApiController
